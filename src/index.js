let slideIndex = 1;
const slides = document.getElementsByClassName("slide-text");
const current = document.querySelector('#current');
const pageNumber = document.querySelector('#page');
const total = document.getElementsByClassName("last");
const pagination = document.getElementsByClassName('slide');

showSlides(slideIndex);

if (slides.length < 6) {
    total.textContent = `0${slides.length}`;
} else {
    total.textContent = slides.length;
}

function nextSlide() {
    showSlides(slideIndex += 1);
}

function showSlides(n) {
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }

    for (let slide of slides) {
        slide.style.display = "none";
    }

    slides[slideIndex - 1].style.display = "grid";

    for (let pag of pagination) {
        pag.style.cssText = ' background: #E5E5E5; width: 4px; height: 2.085rem; ';
    }

    pagination[slideIndex - 1].style.cssText = ' background:  #B4977B; width: 4px; height: 2.085rem; border-radius: 2px;';


    if (slides.length < 6) {
        pageNumber.textContent = `0${slideIndex}`;
        current.textContent = `0${slideIndex}`;

    } else {
        pageNumber.textContent = slideIndex;
        current.textContent = slideIndex;
    }
}
